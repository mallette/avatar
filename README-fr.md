# Comicgen

<!-- markdownlint-disable MD033 -->

<!-- var introduction -->

Nous aimons les BD. Voici donc un cadeau pour tout le monde : un Créateur d'outil de Personnage.
Nous avons créé cet outil pour aider les personnes à illustrer et raconter de meilleures histoires en utilisant la bande dessinée.

Soucieux de développer et de promouvoir les communs numériques, nous hébergeons de nombreux outils sur notre [Mallette numérique](https://mallette.cemea.org)

* [Code source disponible](https://gitlab.cemea.org/mallette/avatar)

----

Cet outil est une version dérivée de 
**[Comicgen](https://gramener.com/comicgen)**, mais traduite et hébergée par les [Ceméa](https://cemea.asso.fr).

<!-- end -->

<!-- var usage -->

## Usage et techniques

Une manière simple d'utiliser l'outil original est de se rendre sur la page [gramener.com/comicgen/](https://gramener.com/comicgen/).

- Choisissez votre personnage et personnalisez-le
- Enregistrez l'image en SVG
- Insérez-là dans votre éditeur favori (LibreOffice Impress, Inkscape...).

### Une interface dédiée pour les Ceméa

Une interface est dédiée à la mise en forme de BD à l'adresse [BD Cemea](https://bd.cemea.org)

Voici une vidéo (en anglais) de 3 minutes qui explique [comment créer sa propre Bande Dessinée](https://invidious.privacyredirect.com/watch?v=E_2hdZuugI8) (ou sa version [Youtube](https://www.youtube.com/watch?v=E_2hdZuugI8))


## Polices

Pour insérer des caractères, vous pouvez utiliser les polices depuis 
[Google fonts](https://fonts.google.com/?category=Handwriting) or
[Fonts.com](https://www.fonts.com/search/all-fonts?Classification=Comic).

Les autres polices que nous aimons :

| Font                              | Example text                                              |
| --------------------------------- | --------------------------------------------------------- |
| [Architects Daughter][font-ad]    | [![Specimen](docs/font-architects-daughter.png)][font-ad] |
| [Cavolini][font-ca] (Windows)     | [![Specimen](docs/font-cavolini.png)][font-ca]            |
| [Segoe Script][font-ss] (Windows) | [![Specimen](docs/font-segoe-script.png)][font-ss]        |
| [Segoe Print][font-sp] (Windows)  | [![Specimen](docs/font-segoe-print.png)][font-sp]         |
| [News Cycle][font-nc]             | [![Specimen](docs/font-news-cycle.png)][font-nc]          |
| [Indie Flower][font-if]           | [![Specimen](docs/font-indie-flower.png)][font-if]        |
| [Amatic SC][font-ac]              | [![Specimen](docs/font-amatic-sc.png)][font-ac]           |
| [Schoolbell][font-sb]             | [![Specimen](docs/font-schoolbell.png)][font-sb]          |
| [Just Another Hand][font-jah]     | [![Specimen](docs/font-just-another-hand.png)][font-jah]  |
| [Patrick Hand][font-ph]           | [![Specimen](docs/font-patrick-hand.png)][font-ph]        |
| [Neucha][font-n]                  | [![Specimen](docs/font-neucha.png)][font-n]               |
| [Handlee][font-h]                 | [![Specimen](docs/font-handlee.png)][font-h]              |

[font-ca]: https://www.fonts.com/font/monotype/cavolini
[font-ad]: https://fonts.google.com/specimen/Architects+Daughter
[font-ss]: https://www.fonts.com/font/microsoft-corporation/segoe-script
[font-sp]: https://www.fonts.com/font/microsoft-corporation/segoe-print
[font-nc]: https://fonts.google.com/specimen/News+Cycle
[font-if]: https://fonts.google.com/specimen/Indie+Flower
[font-ac]: https://fonts.google.com/specimen/Amatic+SC
[font-sb]: https://fonts.google.com/specimen/Schoolbell
[font-jah]: https://fonts.google.com/specimen/Just+Another+Hand
[font-ph]: https://fonts.google.com/specimen/Patrick+Hand
[font-n]: https://fonts.google.com/specimen/Neucha
[font-h]: https://fonts.google.com/specimen/Handlee

## Plugins

Vous pouvez aussi utiliser cet outil avec des [plugins](#plugins) ci-dessous.
(Nous avons prévu de développer [plus de plugins](https://github.com/gramener/comicgen/labels/integrate). Votre aide est la bienvenue !)

<!-- end -->

<!-- var api -->

## REST API (partie technique)

Les personnages de BD sont disponibles depuis un EndPoint `https://avatar.cemea.org/comic` (ou l'endroit où vous l'avez installé).
Nous prenons comme référence `/comic` à partir de cet instant.

Les options pour chaque personnage peuvent être spécifiées comme paramètre de l'URL. Par exemple, pour obtenir Ethan qui se penche sur le côté, en faisant un clin d'œil :

- `name`: `ethan`
- `angle`: `side`
- `emotion`: `wink`
- `pose`: `normal`

Il est disponible depuis `/comic?name=ethan&angle=side&emotion=wink&pose=normal`:

![Ethan sur le côté avec clin d'oeil](comic?name=ethan&angle=side&emotion=wink&pose=normal)

La liste complète des options est disponible dans [dist/characterlist.json](dist/characterlist.json).

Vous pouvez donc créer directement des pesonnages avec ces liens
Vous pouvez intégrer ces images directement dans votre plugin.

## HTML API

Pour intéger les personnages comme un composant HTML, ajoutez ceci à votre page :

```html
<script src="https://cdn.jsdelivr.net/npm/uifactory@1.18.0/dist/uifactory.min.js" import="@comic-gen"></script>
```

Vous pourrez alors ajouter le tag `<comic-gen>` avec les options pour chaque caractère comme attribut, comme ceci :

```html
<comic-gen name="ethan" angle="side" emotion="wink" pose="normal" ext="svg"></comic-gen>
```

Pour obtenir un PNG, remplacez `ext="svg"` par `ext="png"`.

Si vous changez les attributs avec JavaScript, le personne est re-généré.

**NOTE**: Actuellement, `<comic-gen>` ne peut pas être utilisé à l'intérieur d'un élément SVG.

<!-- end -->

<!-- var vision -->

## Pour le StoryTelling

Storytellers want to share a message and change their audience. But they worry that their content is not engaging or "catchy" enough to drive the change.

- **Comics are "catchy"**. That makes them a powerful way of engaging the audience.
- **Comics are simple**. Comics signal that the content is simple, interesting, and funny. Authors often write simpler content for comics -- making it come true.
- **Comics drive emotion**. The pictures convey emotions better than just words alone. They're funny. That helps learning and makes the stories more memorable.

Anyone who writes an email, a presentation, or a document, is a storyteller.

## Comicgen is for developers

Developers want to build engaging apps. But design skills are not their forte. Stock images can't match the variety of their scenarios.

- **Comicgen has variety**. Keeping angles, emotions, and poses independent generates thousands of combinations.
- **Comicgen has an API**. Developers can easily add it to their applications.
- **Comicgen is public**. No need to license characters.

## Organizations use it when presenting or marketing

Organizations typically use Comicgen for:

- **Presenting insights**
  - **Executives' analysis**. An analyst created a poster explaining their work using comic characters. It was simple and engaging -- the entire organization understood this deep learning technique.
  - **Managers' reports**. An admin manager sent his status report as a pair of comic characters conversing. Their CEO read this report fully for the first time.
  - **Consultants' workshops**. A consultant runs a culture workshop using [comics in the presentation](https://www.businessillustrator.com/culture-change-with-comics-workshop/) because "... it's a lot less threatening than an official PowerPoint presentation."
- **Marketing stories**. This could be:
  - **Product teams launching features**. Google Chrome was launched using a [comic book](http://scottmccloud.com/googlechrome/).
  - **Marketer emails**
  - **Event manager invites**

## Comicgen makes storytelling easy

Our vision is to make storytelling with comics easy for everyone. (This includes non-designers, non-programmers, and non-storytellers.)

We do this by:

- [Adding characters](https://github.com/gramener/comicgen/labels/characters).
  Characters can be split into layers (like face & body). By combining these cleverly, we can create more characters with fewer drawings.
  If you have a character idea, please [add a comment](https://github.com/gramener/comicgen/issues/27).
- [Adding layers](https://github.com/gramener/comicgen/labels/layers). We need objects like speech bubbles, panels, headings, objects, scenery, backgrounds etc.
- [Integrate into your workflow](https://github.com/gramener/comicgen/labels/integrate). Comicgen should be easy to use in people's current workflow, with their existing tools.
  - Designers use Illustrator / Sketch
  - Developers use HTML / JS
  - Analysts use Tableau / Power BI
  - Managers use e-mail / MS Office
  - We want to make storytelling easy for everyone
- [API for developers](https://github.com/gramener/comicgen/labels/api). Comicgen automates away the drudgery in creating comics. Developers should be able to create any comic without designing, purely using an API
- [UI for non-developers](https://github.com/gramener/comicgen/labels/builder). We want users to be able to do this without any programming. This means exposing every feature of the API should be exposed on the UI
- [Teaching](https://github.com/gramener/comicgen/labels/teach). The ultimate aim is for people to build better stories. Let's teach them

## We measure success by adoption

We succeed when more people create more and better stories with Comicgen. We measure this by

- How many people have used comicgen
- How many stories have been created using comicgen
- How many characters are present in comicgen. (Variety drives adoption)
- TODO: We need more intermediate success metrics -- things that will drive adoption

<!-- end -->

<!-- var development -->

## Run Comicgen server

Install the dependencies to run Comicgen:

- [Node](https://nodejs.org/en/)
- [Git](https://git-scm.com/)
- [Git LFS](https://git-lfs.github.com/)

```bash
git clone https://github.com/gramener/comicgen
cd comicgen       # Go to the Comicgen folder
npm install       # Install dependencies
npm run build     # Compile Comicgen
npm start         # Run Comicgen server on port 3000
```

## Add new characters

To add a new character, or add images for an existing character:

1. Add the SVG images under `svg/<character>/<attr>/.../<file>.svg`
2. File or folder names must use only lowercase letters. Avoid numbers or special characters
3. Add an `svg/<character>/index.json` and `svg/<character>/index.svg`. See `svg/dee/` for reference
4. Update the [character credits](#character-credits)
5. Run `npm run build` to recompile files under `dist/`
6. Run src/ on the comicgen folder and test the character

## Release

New versions of comicgen are released on [Github](https://github.com/gramener/comicgen/)
and [npm](https://www.npmjs.com/package/comicgen). Here is the release process:

```bash
# Update package.json version.
npm install
npm upgrade
npm run build
npm run lint

export VERSION=1.x.x
git commit . -m"DOC: Release version $VERSION"
git push origin v1
git push gitlab v1
# Then: Test build at https://code.gramener.com/s.anand/deedey/-/pipelines
# Then: Test output at https://gramener.com/comicgen/v1/

# Merge into release branch
git checkout release
git merge v1
git tag -a v$VERSION -m"Release version $VERSION"
git push gitlab release --follow-tags
git push origin release --follow-tags
git checkout v1
```

Then release on [npm](https://www.npmjs.com/package/comicgen) and [Docker](https://hub.docker.com/repository/docker/gramener/comicgen):

```bash
# npm repo owned by @sanand0
npm publish
docker build --tag gramener/comicgen:latest --tag gramener/comicgen:$VERSION pkg/docker/
docker push gramener/comicgen:latest
docker push gramener/comicgen:$VERSION
```

## Help wanted (developers)

If you're a developer, we'd love your help in improving comicgen.

1. **Report bugs**. If something doesn't work the way you expect, please [add an issue](https://github.com/gramener/comicgen/issues)
2. **Ask for features**. Go through the [issues](https://github.com/gramener/comicgen/issues). Add a [Like reaction](https://help.github.com/en/articles/about-conversations-on-github#reacting-to-ideas-in-comments) to what you like. Or add an issue asking for what you want.
3. **Offer help**. Go through these issues. Pick something interesting. Add a comment saying "I'd like to help." We'll revert in 2-4 days with ideas.

There are 3 areas we're focusing on. Help in these areas would be ideal.

### 1. Integrate comicgen into platforms

People like to use their own platforms, not switch to a new one. So let's integrate comicgen into popular platforms like Excel, PowerPoint, Power BI, Tableau, R, etc as plugins.

[See **integration** issues related &raquo;](https://github.com/gramener/comicgen/labels/integrate)

### 2. Create a comic builder UI

People find it easier to create comics using a UI than programming. So let's create an interface that let people create an entire graphic novel!

[See **builder** issues &raquo;](https://github.com/gramener/comicgen/labels/builder)

### 3. Improving comicgen API

Developers access comicgen through a JS library. What can we do to make it easier, and feature rich?

[See **API** issues &raquo;](https://github.com/gramener/comicgen/labels/api)

<!-- end -->

<!-- var credits -->

## Remerciements

Traductions et contributions proposées par :
- Flavien
- Morgane
- François
- Lolo
- Guillaume


Librarie développée par

- Kriti Rohilla <kriti.rohilla@gramenerit.com>
- S Anand <s.anand@gramener.com>
- Shamili Robbi <shamili.robbi@gramener.com>
- Tejesh <tejesh.p@gramener.com>

Conçue et designée par

- Ramya Mylavarapu <ramya.mylavarapu@gramener.com>
- Richie Lionell <richie.lionell@gramener.com>

### Crédits des personnages

- Devarani B <devarani.b@gramener.com>: Aavatar, Ethan & Facesketch
  under [CC0 license](https://creativecommons.org/choose/zero/)
- Jed: Smiley
  under [CC0 license](https://creativecommons.org/choose/zero/)
- Google Inc.: [Noto emoji](https://github.com/googlefonts/noto-emoji)
  under [Apache 2.0](https://github.com/googlefonts/noto-emoji/blob/main/LICENSE) license
- Mike Hoye <mhoye@mozilla.com>: [FXEmoji](https://github.com/mozilla/fxemoji)
  under [CC-BY license](https://github.com/mozilla/fxemoji/blob/gh-pages/LICENSE.md)
- [Pablo Stanley](https://twitter.com/pablostanley): [Humaaans](https://www.humaaans.com/)
  under [CC-BY license](https://creativecommons.org/licenses/by/4.0/)
- Ramya Mylavarapu <ramya.mylavarapu@gramener.com>: Aavatar, Ava, Bean, Biden, Dee, Dey, Evan, Holmes, Jaya, Priya, Ringo, Smiley, Speechbubbles, Trump & Watson
  under [CC0 license](https://creativecommons.org/choose/zero/)
- Renel McCullum: Bill
  under [CC0 license](https://creativecommons.org/choose/zero/)
- Shawf Designs <https://www.fiverr.com/shawf_designs>: Sophie
  under [CC0 license](https://creativecommons.org/choose/zero/)
- Swetha Mylavarapu: Aryan & Zoe
  under [CC0 license](https://creativecommons.org/choose/zero/)
- Suchismita Naik <suchismita.naik@gramener.com>: Ricky
  under [CC0 license](https://creativecommons.org/choose/zero/)

<!-- end -->

<!-- var design -->

## Aide souhaitée (pour dessinateurs·rices)

Dessinateurs·rices, designeur·euse, nous serions ravis de votre aide pour améliorer cet outil.


Vous pouvez :

1. **Dessiner de nouveaux personnages**. Les personnages sont modifiables par toutes et tous. Nous vous mentionnerons dans [les crédits](#credits). Vous pouvez dessiner votre propre personnage ou choisir parmi [la liste des personnages que nous recherchons](https://github.com/gramener/comicgen/labels/characters).
2. **Ajouter de nouvelles couches**. En complément de personnages, vous pouvez créer des "couches" -- les éléments que vous pouvez ajouter, comme les bulles, les fonds, les objets, etc. Vous pouvez dessiner de nouvelles parties d'objets qui peuvent être utiles. Voici quelques chouches [demandées par d'autres personnes](https://github.com/gramener/comicgen/labels/layers).

Voici un guide pour mieux comprendre comment dessiner et soumettre de nouveaux personnages ou couches.

## Dessiner de nouveaux avatars

Les personnages sont fabriqués à partir de 1 ou plusieurs fichiers SVG.

Le moyen le plus facile de créer un personnage est de créer une douzaine de fichiers SVG et de les sauvegarder de manière individuelle, **à la même dimension**. Par exemple :

![Series d'images SVG pour un personnage](docs/character-single-images.png)

Une manière simple serait de présenter un personnage en différentes parties. Par exemple, vous pouvez dessiner des visages avec différentes émotions et les enregistrer chacune dans un dossier `faces/`:

![Visages pour un personnage](docs/character-faces.png)

Vous pouvez alors dessiner des corps différents et les enregistrer dans un dossier  `bodies/`:

![Association corps et personnages](docs/character-bodies.png)

Lorsque vous souhaitez les associer, assurez-vous que :

- tous les visages aient les **mêmes dimensions**, et soient à la même position dans le SVG
- Tous les corps soient à la même dimension et à la même position dans le SVG
- Quand vous superposez n'importe quel visage avec n'importe quel corps, les **images doivent s'aligner**.

Vous pouvez choisir d'associer les images de n'importe quelle manière. Par exemple :

- `faces/`, `bodies/`
- `face/`, `trunk/`, `leg/`, `shoes/`
- `hair/`, `face/`, `eyes/`, `mouth/`, `trunk/`, `legs/`

Plus il y a de combinaisons, et plus les images deviennent complexes. Vous devriez commencer modestement et ajouter ensuite de la variété.

Lorsque vous créez un personnage, gardez ces éléments à l'esprit :
 
- Commencez avec un dessin de **500px x 600px**.
- Réduisez le nombre de points au minimum. Ils sont faciles à éditer et cela réduit la taille du fichier.
- Gardez une **taille de trait constante** sur toutes les parties du corps.
- Gardez chaque SVG d'une taille **inférieure à 10kb**.
- Réutilisez les couleurs. Limitez-vous à 5 couleurs par personnage.
  - Utilisez l'opacité pour les ombres, le fard à joues, les larmes et l'éclairage. Lorsque les couleurs changent, elles se fondent dans le décor. 
- Nommez chaque fichier SVG en minuscules sans espaces. Évitez les majuscules et les caractères spéciaux.
  - Par exemple, `lookingdown.svg` est correct. `LookingDown.svg`, `looking-down.svg` ou `looking down.svg` ne le sont pas. 
 
Utilisez le modèle ci-dessous comme base pour créer votre personnage.
 
![Modèle de base](docs/base-template.svg)
 
- Le corps doit partir de la ligne horizontale, le centre du cou étant aligné sur la ligne verticale. Le visage
  doit être positionné sur la ligne horizontale et doit entrer dans la boîte. Les cheveux et les oreilles peuvent
  peuvent sortir de la boîte.
 
[Modèle de base avec corps et visage](docs/base-template-body-face.svg)
 
- Toutes les poses doivent être **légèrement inclinées** comme indiqué ci-dessous
 
![Pose latérale](docs/side-pose.svg)
                                                                                                                                                                             
### Enregistrement du fichier
 
Aller dans Fichier / Exporter sous / Format : SVG
 
![Options d'enregistrement d'Adobe Illustrator](docs/adobe-illustrator-save.png) 

### exemple de list des émotions des personnages
 
- `apeuré`
- `en colère`
- `ennuyé`
- `rougissant`
- `confus`
- `en pleurs`
- `crier à haute voix`
- `courageux`
- `curieux` 
- `désappointé`
- `dozing`
- `saoul`
- `excité`
- `facepalm`
- `heureux`
- `coups d'œil`
- `irrité`
- `regard vers le bas`
- `regard à gauche`
- `regard à droite`
- `regard vers le haut`
- `masqué`
- `neutre`
- `sans réfléchir`
- `ooh`
- `mort de rire`
- `les yeux au ciel`
- `triste`
- `effrayé`
- `choqué`
- `cris`
- `sourire`
- `smirk`
- `étonné`
- `surpris`
- `pensif`
- `fatigué`
- `tirant la langue`
- `sifflant`
- `clin d'oeil`
- `inquiet`

### Proposer de nouveaux personnagges

Donnez un nom à votre personnage et enregistrez les fichiers SVG dans un dossier avec le nom du personnage en minuscule.

Utiliser le [formulaire](https://mallette.cemea.org/nous-contacter)  pour nous contacter.

<!-- end -->

<!-- var social -->
<div class="d-flex justify-content-between">
</div>
<!-- end -->

<!-- var privacypolicy -->

### Privacy Policy

Gramener visuals do not externally collect any information, personal or otherwise.
If you have any questions, please contact us at [comicgen.powerbi@gramener.com](mailto:comicgen.powerbi@gramener.com)

<!-- end -->

<!-- var social_markdown -->

<!-- Github README won't display the above share icons. So create links. Don't display this on index.html -->

## Share

- [Discuss on Twitter. Hashtag #comicgen](https://twitter.com/search?f=tweets&vertical=default&q=comicgen&src=typd)
- [Share on Twitter](https://twitter.com/intent/tweet?text=Make%20your%20own%20comics%20with%20the%20%23comicgen%20JS%20API%20by%20%40Gramener%20https%3A%2F%2Fgramener.com%2Fcomicgen%2F)
- [Share on Facebook](https://www.facebook.com/dialog/share?app_id=163328100435225&display=page&href=https%3A%2F%2Fgramener.com%2Fcomicgen%2F&redirect_uri=https%3A%2F%2Fgramener.com%2Fcomicgen%2F&quote=Make%20your%20own%20comics%20with%20the%20%23comicgen%20JS%20API%20by%20%40Gramener%20https%3A%2F%2Fgramener.com%2Fcomicgen%2F)
- [Share on LinkedIn](https://www.linkedin.com/sharing/share-offsite/?url=https://gramener.com/comicgen/)
- [Discuss on Hacker News](https://news.ycombinator.com/item?id=20049116)
- [Fork on Github](https://github.com/gramener/comicgen)
<!-- end -->
