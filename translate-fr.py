#!/usr/bin/python

import os, re
import pdb

# On ne change que ce qui est dans topdir
topdir='svg'
extsvg='.svg'

# Script de conversion des anglicismes en fr
# de https://gramener.com/comicgen/v1/
sentencetranslate = {
    "gender": "genre",
    "female": "femme",
    "male": "homme",
    "unisex": "non genré",
    "character": "personnage",
    "bindi": "point rouge",
    "blondecurly": "blonde bouclée",
    "blondeshort": "blond court",
    "densehair": "cheveux denses",
    "densehair with band": "cheveux denses avec bandeau",
    "hairband": "bandeaux pour cheveux",
    "highbun": "chignon haut",
    "messyponytail": "queue de cheval désordonnée",
    "oldladywithglasses": "vieille dame avec lunette",
    "shorthair": "cheveux court",
    "wavy": "ondulés",
    "expression": "expression",
    "pose": "pose"
    }

dirtranslate = {
        }

def replace_files(chainecherchee, chaineremplacee):
    """remplace tous les fichiers svg par leur traduction"""
    for dirpath, dirnames, files in os.walk(topdir):
        for file in files:
            if str(chainecherchee)+extsvg == str(file):
                print('Dans le chemin "'+dirpath+'", on renomme le fichier "'+chainecherchee+'" en "'+chaineremplacee+'"')
                print('file :'+file)
                # print(os.path.join(dirpath, dir))
                # TODO : renommer le fichier proprement

# listetrouves = (find_all("sitting","."))

def replace_dirs(chainecherchee,chaineremplacee):
    """remplace tous les noms de dossier recherche par un autre mot traduit"""
    # topdir = 'svg'
    for dirpath, dirnames, files in os.walk(topdir):
        for dir in dirnames:
            if str(dir) == str(chainecherchee):
                print('Dans le chemin "'+dirpath+'", on renomme le dossier "'+chainecherchee+'" en "'+chaineremplacee+'"')
                # print(os.path.join(dirpath, dir))
                # TODO action de renommer

def list_files():
    """liste tous les fichiers présents, sans doublon"""
    listfiles = []
    for dirpath, dirnames, files in os.walk(topdir):
        for file in files:
            chercher = re.match("^(.+?)\.svg$", str(file))
            if chercher:
                namefile = chercher[1]
                # print(namefile)
                if not(str(namefile) in listfiles):
                    listfiles.append(namefile)
                # print('"'+file+'": "",')
                # print(os.path.join(dirpath, dir))
                # TODO action de renommer
    listfiles.sort()
    for file in listfiles:
        print('"'+file+'": "",')
    print('# Nombre de fichiers : '+str(len(listfiles)))
    #return listfiles

def list_dirs():
    """liste tous les dossiers présents, sand doublon"""
    listdir = []
    for dirpath, dirnames, files in os.walk(topdir):
        for dir in dirnames:
            if not(str(dir) in listdir):
                listdir.append(dir)
                # TODO action de renommer
    listdir.sort()
    for dir in listdir:
        print('"'+dir+'": "",')
    print('# Nombre de dossiers : '+str(len(listdir)))
    #return listfiles

def list_all():
    print('filetranslate= {')
    list_files()
    print('}')
    print()
    print('# **************************************************')
    print()
    print('dirtranslate= {')
    list_dirs()
    print('}')

def __main__():
    # On lance la recherche sur toutes les chaines
    # for cle, valeur in sentencetranslate.items():
        # print("l'élément de clé", cle, "vaut", valeur)
        #replace_dirs(cle,valeur)
        # replace_files(cle,valeur)
    list_all()

__main__()

# os.rename('a.txt', 'b.kml')

# old_file = os.path.join("directory", "a.txt")
# new_file = os.path.join("directory", "b.kml")
# os.rename(old_file, new_file)


